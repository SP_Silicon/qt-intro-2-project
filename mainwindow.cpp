#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->addClockButton, &QPushButton::clicked, this, &MainWindow::addClock);

    // Get and list all timezones in dropdown
    auto timezones = QTimeZone::availableTimeZoneIds(QLocale::UnitedStates);
    for (auto& timezone : timezones) {
        ui->timezoneBox->addItem(timezone);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addClock() {

    // get current timezone as bytearray
    QByteArray timezone = ui->timezoneBox->currentText().toUtf8();

    // create new clock with that timezone
    CustomClock* clock = new CustomClock(timezone);

    // put it in the specific location
    ui->clockLayout->addWidget(clock, nextLocation.first, nextLocation.second, 1, 1);

    // Update location of next widget
    nextLocation.first++;
    if(nextLocation.first == 3) {
        nextLocation.first = 0;
        nextLocation.second++;
    }
}
