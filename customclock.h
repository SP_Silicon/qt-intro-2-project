#ifndef CUSTOMCLOCK_H
#define CUSTOMCLOCK_H

#include <QWidget>
#include <QByteArray>
#include <QTimer>
#include <QDateTime>
#include <QTimeZone>

namespace Ui {
class CustomClock;
}

class CustomClock : public QWidget
{
    Q_OBJECT

public:
    explicit CustomClock(const QByteArray& timezone, QWidget *parent = nullptr);
    ~CustomClock();

public slots:
    void updateTimer();

private:
    Ui::CustomClock *ui;
    QTimer *timer;
    QTimeZone clockZone;
};

#endif // CUSTOMCLOCK_H
