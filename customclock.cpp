#include "customclock.h"
#include "ui_customclock.h"

CustomClock::CustomClock(const QByteArray& timezone_name, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CustomClock)
{
    ui->setupUi(this);

    // Set the timezone to the specific timezone and display it
    ui->timezoneLabel->setText(timezone_name);
    clockZone = QTimeZone(timezone_name);

    // declare the range and format of the seconds bar. Then set first time
    ui->secondsBar->setRange(0, 60);
    ui->secondsBar->setFormat("%v seconds");
    updateTimer();

    // create timer object that will run updateTimer every second
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout,this, &CustomClock::updateTimer);
    timer->start(1000);
}

CustomClock::~CustomClock()
{
    delete ui;
}



// Function to run every x seconds
void CustomClock::updateTimer()
{
    // get current time in specific timezone
    QDateTime clockTime = QDateTime::currentDateTimeUtc().toTimeZone(clockZone);

    // get current time object
    QTime time = clockTime.time();

    //format to hour : minute
    QString timeString = time.toString("hh:mm");

    // If time is even, don't show colon
    timeString[2] = (time.second() % 2) == 0 ? ' ' : ':';

    // Set LCD to time and seconds bar to seconds
    ui->timeLCD->display(timeString);
    ui->secondsBar->setValue(time.second());

}
